// Node.js API
const http = require('http');
const url = require('url');
const fs = require('fs');
const path = require('path');

// Path to static web content directory
const staticDir = path.join(__dirname, 'static');

// JSON files
const settings = require(path.join(__dirname, 'settings.json'));
const mimeTypes = require(path.join(__dirname, 'mime_types.json'));

// Read a file and return a HTTP status code and file content
async function readFile(filePath) {
    return new Promise((resolve, reject) => { // Wrap fs.readFile in Promise to use async/await
        fs.readFile(filePath, (error, data) => {

            if (error) {
                console.log('File reading error:', error);
                if (error.code === 'ENOENT') {
                    reject([404, null]);
                } else {
                    reject([500, null]);
                }
            }

            resolve([200, data]);
        });
    });
}

// Return an object representing the processed GET request
async function getRequest(reqUrl) {
    const filePath = path.join( // Join path to static dir with requested URL
        staticDir,
        (reqUrl.pathname === '/') // If URL is '/' change to 'index.html'
        ? 'index.html'
        : reqUrl.pathname
    );

    let statusCode, statusMessage, contentType, content, extname;

    try {
        [statusCode, content] = await readFile(filePath);
        extname = String(path.extname(filePath))
                  .toLowerCase();
        contentType = mimeTypes[extname] || mimeTypes['default']; // Set content type based on extension, otherwise fallback on default: application/octet-stream

    } catch (error) {
        [statusCode, readFileError] = error;
        console.log('GET request error:', {
            url: reqUrl.path,
            statusCode: statusCode
        });
        contentType = mimeTypes['.txt']; // Error message to client sent as plain/text
    }

    return {
        statusCode: statusCode,
        statusMessage: http.STATUS_CODES[statusCode],
        contentType: contentType,
        content: content // 'undefined' if there was an error reading the requested file
    };
}

// Handle incoming requests to the server
async function requestHandler(req, res) {
    const reqUrl = url.parse(req.url, true); // The 'true' setting separates the request query string into key/value pairs

    if (req.method.toLowerCase() === 'get') {
        const data = await getRequest(reqUrl)

        res.writeHead(
            data.statusCode,
            data.statusMessage,
            {
                'Content-Type': data.contentType
            }
        );

        res.end(data.content || `${data.statusCode} ${data.statusMessage}`); // If no file content exists, send status code and status message to client instead
    }
}

http.createServer(requestHandler).listen(settings.port);